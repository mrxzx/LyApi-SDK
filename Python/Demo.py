import LyApi

def main():

    # 访问接口的配置，让程序知道如何正常的访问接口
    config = {
        "url": "http://lyapi.org/server/public/?service=Demo.Hello",
        "service": "GET",
        "service_alisa": "service",
        "service_division": "."
    }

    lyapi = LyApi.LyApi(config)

    # 一个简单的数据请求，请求 Demo 对象下的 Hello 函数
    EasyGetData = lyapi.GetApiData('Demo', 'Hello',{},'json')

    print(EasyGetData)

if __name__ == '__main__':
    main()
    