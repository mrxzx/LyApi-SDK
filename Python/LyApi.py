# LyApi-SDK For Python

# Author: MrXiaoZhuoX
# Version: V1.0
# License: MIT License

import requests,json

class LyApi(object):

    # 默认程序配置
    URL = ''
    Servcie = 'URL'
    ServcieAlias = 'service'
    ServcieDivision = '.'

    # 构造函数：主要处理配置信息
    def __init__(self,config):

        # 处理SDK配置信息
        if isinstance(config,str):
            self.URL = config
        if isinstance(config,dict):

            if 'url' in config:
                self.URL = config['url']

                # 处理URL的一些特殊情况
                if self.URL[-1:] != '/':
                    self.URL += '/'

            if 'service' in config:
                self.Servcie = config['service']

            if 'service_alisa' in config:
                self.ServcieAlias = config['service_alisa']

            if 'service_division' in config:
                self.ServcieDivision = config['service_division']

    # 最简单的获取数据函数 (只将数据返回)
    def GetApiData(self,obj,func,data = {},type = 'objcet'):

        Object_URL = self.URL
        Object_Service = self.Servcie
        Object_Data = data


        if Object_URL != '' :

            if Object_Service.upper() == 'URL':
                Object_URL += obj + "/" + func
            else:
                # 处理命名空间特殊原因
                obj = obj.replace('/',self.ServcieDivision)
                obj = obj.replace('//',self.ServcieDivision)

                Object_Data[self.ServcieAlias] = obj + self.ServcieDivision + func

            # 根据需求返回不同的内容
            if type == 'object':
                Respnse_Json = requests.get(Object_URL, Object_Data).json()
                return json.loads(Respnse_Json)
            else:
                return requests.get(Object_URL, Object_Data).json()